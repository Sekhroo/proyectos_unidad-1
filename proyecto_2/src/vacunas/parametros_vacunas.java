package vacunas;

public class parametros_vacunas {
	 
	private int cantidad_max;
	
	private int cantidad_vacuna_1;
	private int	cantidad_vacuna_2;
	private int cantidad_vacuna_3;
	
	private int otorgar;
	
	public parametros_vacunas(int cantidad_max,int proporcion_1,int proporcion_2,int proporcion_3, int otorgar) {

		this.cantidad_max = cantidad_max/2;
		
		double proporcion = proporcion_1 + proporcion_2 + proporcion_3;
		double coeficiente_proporcion = this.cantidad_max/proporcion;
		
		this.cantidad_vacuna_1 = (int)(proporcion_1*coeficiente_proporcion);
		this.cantidad_vacuna_2 = (int)(proporcion_2*coeficiente_proporcion);
		this.cantidad_vacuna_3 = (int)(proporcion_3*coeficiente_proporcion);
		
		this.otorgar = otorgar;
		
	}
	
    public int getcantidad_vacuna_1() {
        return this.cantidad_vacuna_1;
    }
    public void setcantidad_vacuna_1(int cantidad_vacuna_1) {
        this.cantidad_vacuna_1 = cantidad_vacuna_1;
    }
    
    public int getcantidad_vacuna_2() {
        return this.cantidad_vacuna_2;
    }
    public void setcantidad_vacuna_2(int cantidad_vacuna_2) {
        this.cantidad_vacuna_2 = cantidad_vacuna_2;
    }
    
    public int getcantidad_vacuna_3() {
        return this.cantidad_vacuna_3;
    }
    public void setcantidad_vacuna_3(int cantidad_vacuna_3) {
        this.cantidad_vacuna_3 = cantidad_vacuna_3;
    }
    
    public int getotorgar() {
        return this.otorgar;
    }
    public void setotorgar(int otorgar) {
        this.otorgar = otorgar;
    } 
}