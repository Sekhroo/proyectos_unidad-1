package vacunas;

public class vacuna {
	
	private int vacuna;
	private int pasos_segunda_dosis;
	private int efecto_prob_muerte;
	private int cantidad;
	
	public vacuna(int vacuna, int pasos_segunda_dosis, int efecto_prob_muerte, int cantidad) {
		this.vacuna = vacuna;
		this.pasos_segunda_dosis = pasos_segunda_dosis;
		this.efecto_prob_muerte = efecto_prob_muerte;
		this.cantidad = cantidad;
	}
	
    public int getvacuna() {
        return this.vacuna;
    }
    public void setvacuna(int vacuna) {
        this.vacuna = vacuna;
    }
    
    public int getpasos_segunda_dosis() {
        return this.pasos_segunda_dosis;
    }
    public void setpasos_segunda_dosis(int pasos_segunda_dosis) {
        this.pasos_segunda_dosis = pasos_segunda_dosis;
    }
    
    public int getefecto_prob_muerte() {
        return this.efecto_prob_muerte;
    }
    public void setefecto_prob_muerte(int efecto_prob_muerte) {
        this.efecto_prob_muerte = efecto_prob_muerte;
    }
    
    public int getcantidad() {
        return this.cantidad;
    }
    public void setcantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}