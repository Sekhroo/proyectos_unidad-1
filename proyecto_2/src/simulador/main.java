package simulador;

public class main {

	public static void main(String[] args) {
		simulador proceso = new simulador();
		proceso.generarcomunidad(); // genera la comunidad y las personas; genera los infectados iniciales
		proceso.evaluacion_inicial(); // dia 0 de la infecion (o paso 0); entrega la suma de los primeros infectados y los casos activos
		proceso.pasos(); // desarrollo de la simulacion
		proceso.evaluacion_final(); // cuantas personas vivieron y cuantas se volvieron inmunes al agente infeccioso
	}
}