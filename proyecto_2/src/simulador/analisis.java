package simulador;

import enfermedades.*;
public class analisis {
	
	// ----------------------------------------------------------------------------
	// Analisis Edad
	// ----------------------------------------------------------------------------
	
	public int rangos_edad(int prob) {
		int min_edad = 0;
		int max_edad = 0;
		if(prob <= 15) {
			min_edad = 0;
			max_edad = 18;
		}if(15 < prob && prob <= 40) {
			min_edad = 19;
			max_edad = 34;
		}if(40 < prob && prob <= 80) {
			min_edad = 35;
			max_edad = 59;
		}if(80 < prob && prob <= 95) {
			min_edad = 60;
			max_edad = 79;
		}else if(95 < prob) {
			min_edad = 80;
			max_edad = 100;
		}
		max_edad = max_edad - min_edad;
		max_edad = (int) (Math.random()*max_edad);
		int edad = max_edad + min_edad;
		return edad;
	}
	
	public int analizar_edad_prob_muerte(int edad) {
		int prob_muerte = 0;
		
		if(edad <= 5) {
			prob_muerte = 40;
		}if(6 <= edad && edad <= 18) {
			prob_muerte = 20;
		}if(19 <= edad && edad <= 34) {
			prob_muerte = 25;
		}if(35 <= edad && edad <= 59) {
			prob_muerte = 30;
		}if(60 <= edad && edad <= 79) {
			prob_muerte = 40;
		}else if(80 <= edad) {
			prob_muerte = 45;
		}
		return prob_muerte;
	}
	
	
	
	// ----------------------------------------------------------------------------
	// Analisis enfermedad base
	// ----------------------------------------------------------------------------
	
	public int asignar_enfer_base(int prob,enfer_base enfer_base_asma, enfer_base enfer_base_ecv, enfer_base enfer_base_fq, enfer_base enfer_base_hiper, enfer_base enfer_base_pal) {
		int prob_cerebro_vascular = enfer_base_asma.getprob_infec_inicial() + enfer_base_ecv.getprob_infec_inicial();
		int prob_febrosis_quistica = prob_cerebro_vascular + enfer_base_fq.getprob_infec_inicial();
		int prob_hipertension = prob_febrosis_quistica + enfer_base_hiper.getprob_infec_inicial();
		int prob_presion_a_alta = prob_hipertension + enfer_base_pal.getprob_infec_inicial();
		
		int enfer_base = 1;
		if(prob <= enfer_base_asma.getprob_infec_inicial()) {
			enfer_base = 2;
		}if(enfer_base_asma.getprob_infec_inicial() < prob && prob <= prob_cerebro_vascular) {
			enfer_base = 3;
		}if(prob_cerebro_vascular < prob && prob <= prob_febrosis_quistica) {
			enfer_base = 4;
		}if(prob_febrosis_quistica < prob && prob <= prob_hipertension) {
			enfer_base = 5;
		}else if(prob_hipertension < prob && prob <= prob_presion_a_alta) {
			enfer_base = 6;
		}	
		return enfer_base;
	}
	
	public int analizar_enfer_base(int enfer_base, enfer_base muerte_asma, enfer_base muerte_cv, enfer_base muerte_fq, enfer_base muerte_hiper, enfer_base muerte_pal) {
		int proba_muerte_nueva = 0;
		
		if (enfer_base == 2) {
			proba_muerte_nueva = 0 + muerte_asma.getprob_morir();
		}if (enfer_base == 3) {
			proba_muerte_nueva = 0 + muerte_cv.getprob_morir();
		}if (enfer_base == 4) {
			proba_muerte_nueva = 0 + muerte_fq.getprob_morir();
		}if (enfer_base == 5) {
			proba_muerte_nueva = 0 + muerte_hiper.getprob_morir();
		}if (enfer_base == 6) {
			proba_muerte_nueva = 0 + muerte_pal.getprob_morir();
		}
				
		return proba_muerte_nueva;
	}
	
	
	
	// ----------------------------------------------------------------------------
	// Analisis afeccion
	// ----------------------------------------------------------------------------
	
	public int asignar_afeccion(int prob, afecciones afecc_obs, afecciones afecc_des) {
		int prob_max = afecc_obs.getprob_infec_inicial() + afecc_des.getprob_infec_inicial();
		
		int afeccion = 1;
		if(prob <= afecc_obs.getprob_infec_inicial()) {
			afeccion = 2;
		}if(afecc_obs.getprob_infec_inicial() < prob && prob <= prob_max) {
			afeccion = 3;
		}
		return afeccion;
	}
	
	public int analizar_afeccion(int afeccion, int proba_muerte, afecciones afecc_obs, afecciones afecc_des) {
		int proba_muerte_nueva = 0;
		if (afeccion == 1) {
			proba_muerte_nueva = 0 + proba_muerte;
		}if (afeccion == 2) {
			proba_muerte_nueva = afecc_obs.getprob_morir() + proba_muerte;
		}if (afeccion == 3) {
			proba_muerte_nueva = afecc_des.getprob_morir() + proba_muerte;
		}
				
		return proba_muerte_nueva;
	}
}