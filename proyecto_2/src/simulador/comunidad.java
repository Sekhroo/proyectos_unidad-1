package simulador;

public class comunidad {

    private int num_perso; // el total de ciudadanos de prueba
    private int pmd_conec_fsc; // cada persona tedra posibilidad de 1 o mas contactos fisicos definida por una probabilidad
    private int num_infec; // su setter esta enlazado a una probabilidad, se encuentra en evaluacion_inicial
    private int prob_conec_fsc; // probabilidad de contacto fisico
    private int num_comunidad; // numero de comunidades en total
    private int prob_conec_fsc_comunidad; //probabilidad de imfeccion en comunidad
    
    public comunidad(int num_perso, int prob_conec_fsc, int prob_conec_fsc_comunidad){
    	this.num_perso = num_perso;
    	this.prob_conec_fsc = prob_conec_fsc;
    	this.num_comunidad = 1;
    	this.prob_conec_fsc_comunidad = prob_conec_fsc_comunidad;
    }
    
	// ----------------------------------------------------------------------------
	// getters y setters
	// ----------------------------------------------------------------------------

    //Get y Set de num_ciudadanos
    public int getnum_perso() {
        return this.num_perso;
    }
    public void setnum_perso(int num_perso) {
        this.num_perso = num_perso;
    }
    
    //Get y Set de promedio_conexion_fisica
    public int getpmd_conec_fsc() {
        return this.pmd_conec_fsc;
    }
    public void setpmd_conec_fsc(int pmd_conec_fsc) {
        this.pmd_conec_fsc = pmd_conec_fsc;
    }
    
    //Get y Set de num_infectados
    public int getnum_infec() {
        return this.num_infec;
    }
    public void setnum_infec(int num_infec) {
        this.num_infec = num_infec;
    }
    
    //Get y Set de probabilidad_conexion_fisica
    public int getprob_conec_fsc() {
        return this.prob_conec_fsc;
    }
    public void setprob_conec_fsc(int prob_conec_fsc) {
        this.prob_conec_fsc = prob_conec_fsc;
    }
    
    //Get y Set total de comunidades
    public int getnum_comunidad() {
        return this.num_comunidad;
    }
    public void setnum_comunidad(int num_comunidad) {
        this.num_comunidad = num_comunidad;
    }
    
  //Get y Set probabilidad de conexion fisica en comunidades
    public int getprob_conec_fsc_comunidad() {
        return this.prob_conec_fsc_comunidad;
    }
    public void setprob_conec_fsc_comunidad(int prob_conec_fsc_comunidad) {
        this.prob_conec_fsc_comunidad = prob_conec_fsc_comunidad;
    }
}