package simulador;

public class ciudadano {

	private int comunidad; // todos forman parte de la misma comunidad
	private int edad;
	private int id; // su posicion en la comunidad (en el arraylist)
	private int infectado; // 1 = no infectado; 2 = infectado; 3 = fuera del simulador
	private int enfer_base;
	private int afeccion;
	private boolean estado; // true = vivo; false = muerto
	private int tiempo_infec; // tiempo que lleva la persona infectada
	private int prob_morir; // probabilidad de que esta persona muera
	private int inmaculado;
	
	
	// ----------------------------------------------------------------------------
	//	Constructor
	// ----------------------------------------------------------------------------
	
    public ciudadano(int id,int edad){
    	this.comunidad = 0;
    	this.edad = edad;
    	this.id = id;
    	this.infectado = 1;
    	this.tiempo_infec = 0;
    	this.estado = true;
    	this.prob_morir = 0;
    	this.enfer_base = 1;
    	this.afeccion = 1;
    	this.inmaculado = 0;
    }
	
	// ----------------------------------------------------------------------------
	// getters y setters
	// ----------------------------------------------------------------------------
    
    //Get y Set de comunidad
    public int getcomunidad() {
        return this.comunidad;
    }
    public void setcomunidad(int comunidad) {
        this.comunidad = comunidad;
    }
    
  //Get y Set de enfermedad base
    public int getenfer_base() {
        return this.enfer_base;
    }
    public void setenfer_base(int enfer_base) {
        this.enfer_base = enfer_base;
    }
    
  //Get y Set de comunidad
    public int getafeccion() {
        return this.afeccion;
    }
    public void setafeccion(int afeccion) {
        this.afeccion = afeccion;
    }
    
    //Get y Set de id
    public int getid() {
        return this.id;
    }
    public void setid(int id) {
        this.id = id;
    }
    
    //Get y Set de edad
    public int getedad() {
        return this.edad;
    }
    public void setedad(int edad) {
        this.edad = edad;
    }
    
    //Get y Set de enfermedad
    public int getinfectado() {
        return this.infectado;
    }
    public void setinfectado(int infectado) {
        this.infectado = infectado;
    }
    
    //Get y Set de estado
    public boolean getestado() {
        return this.estado;
    }
    public void setestado(boolean estado) {
        this.estado = estado;
    }
    
    //Get y Set de enfermedad
    public int gettiempo_infec() {
        return this.tiempo_infec;
    }
    public void settiempo_infec(int tiempo_infec) {
        this.tiempo_infec = tiempo_infec;
    }
    
    //Get y Set de la probabilidad de morir del objeto persona
    public int getprob_morir() {
        return this.prob_morir;
    }
    public void setprob_morir(int prob_morir) {
        this.prob_morir = prob_morir;
    }
    
    //Get y Set de el estado de vacunacion de la persona
    public int getinmaculado() {
        return this.inmaculado;
    }
    public void setinmaculado(int inmaculado) {
        this.inmaculado = inmaculado;
    }
}