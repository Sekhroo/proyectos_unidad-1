package simulador;

import enfermedades.*;
import probabilidad_azar.*;
import pasos.*;
import vacunas.*;

public class simulador {
	
	private pasos steps;
	private comunidad comun;
	
	private enfer_infecciosa enfer;
	
	private enfer_base enfer_base;
	private enfer_base enfer_base_asma;
	private enfer_base enfer_base_ecv;
	private enfer_base enfer_base_fq;
	private enfer_base enfer_base_hiper;
	private enfer_base enfer_base_pal;
	
	private afecciones afecc;
	private afecciones afecc_obs;
	private afecciones afecc_des;
	
	private parametros_vacunas par_vac;
	private vacuna vac1;
	private vacuna vac2;
	private vacuna vac3;
	
	private ciudadano comunidad[];
	private probabilidad azar;
	private analisis obser;

	
	public simulador() {
		this.steps = new pasos(30,7,8);
		//cantidad maxima de pasos
		//tiempo minimo de caso activo
		//tiempo que pasa un leve para transformarse en grave
		
		this.comun = new comunidad(10000,15,85);
		//cantidad maxima de la muestra
		//probabilidad de contacto fisico extra familia
		//probabilidad de contacto fisico inter familia
		
		this.enfer = new enfer_infecciosa(1,1,70,10);
		//probabiliad de infeccion inicial
		//Estado sano
		//probabilidad de infeccion
		//probabilidad de generar inmunidad
		
		this.enfer_base = new enfer_base("Enfermedad_base",25,1,0);
		//Enfermedad base: cualquiera
		//probabilidad de padecer enfermedad incial
		//Estado: ninguna enfermedad base
		//Probabilidad de morir: base sin enfermedad
		
		this.enfer_base_asma = new enfer_base("Asma",20,2,30);
		this.enfer_base_ecv = new enfer_base("Enfermedad cerebrovascular",20,3,15);
		this.enfer_base_fq = new enfer_base("Fribrosis quistica",20,4,10);
		this.enfer_base_hiper = new enfer_base("Hipertension",20,5,15);
		this.enfer_base_pal = new enfer_base("Presion arterial alta",20,6,15);
		//Enfermedad base: cualquiera
		//probabilidad de padecerla de tenerla se padece una enfermedad inicial (la suma de las 5 debe dar 100)
		//Estado: padece enfermedad
		//Probabilidad de morir: con la enfermedad
		
		
		this.afecc = new afecciones("Afecciones",65,1,0);
		//Afeccion
		//probabilidad de padecer afeccion inicial
		//EStado: ninguna afeccion
		//Probabilidad de morir: base sin afeccion
		
		this.afecc_obs = new afecciones("Obesidad",65,2,25);
		this.afecc_des = new afecciones("Desnutricion",35,3,20);
		//Afeccion base: cualquiera
		//probabilidad de padecer afeccion inicial
		//EStado: padece la afeccion
		//Probabilidad de morir: con la afeccion
		
		this.par_vac = new parametros_vacunas(comun.getnum_perso(),9,6,3,10);
		//Total de habitantes en la poblacion
		//razon a
		//razon b
		//razon c
		//pasos para que se entreguen las vacunas
		
		this.vac1 = new vacuna(1,0,25,par_vac.getcantidad_vacuna_1());
		this.vac2 = new vacuna(2,3,0,par_vac.getcantidad_vacuna_2());
		this.vac3 = new vacuna(3,6,0,par_vac.getcantidad_vacuna_3());
		
		this.comunidad = new ciudadano[comun.getnum_perso()];
		
		this.azar = new probabilidad();
		
		this.obser = new analisis();
	}
	
	// ----------------------------------------------------------------------------
	// genera la comunidad y los ciudadanos que la componen, tambien los infectados iniciales con una probabilidad
	// ----------------------------------------------------------------------------
	
	public void generarcomunidad() {

		//infeccion inicial
		for (int i = 0; i < comun.getnum_perso(); ++i) {
			int prob = azar.probabilidad();
			int edad = obser.rangos_edad(prob);
			comunidad[i] = new ciudadano(i+1,edad);
			
			int prob_muerte = obser.analizar_edad_prob_muerte(comunidad[i].getedad());
			comunidad[i].setprob_morir(prob_muerte);
			
			prob = azar.probabilidad();
			if(prob <= enfer_base.getprob_infec_inicial()) {
				prob = azar.probabilidad();
				int enfer_base = obser.asignar_enfer_base(prob,enfer_base_asma,enfer_base_ecv,enfer_base_fq,enfer_base_hiper,enfer_base_pal);
				comunidad[i].setenfer_base(enfer_base);
			}
			prob_muerte = obser.analizar_enfer_base(comunidad[i].getenfer_base(),enfer_base_asma,enfer_base_ecv,enfer_base_fq,enfer_base_hiper,enfer_base_pal);
			comunidad[i].setprob_morir(comunidad[i].getprob_morir()+prob_muerte);
			
			prob = azar.probabilidad();
			if(prob <= afecc.getprob_infec_inicial()) {
				prob = azar.probabilidad();
				int afeccion = obser.asignar_afeccion(prob,afecc_obs,afecc_des);
				comunidad[i].setafeccion(afeccion);
			}
			prob_muerte = obser.analizar_afeccion(comunidad[i].getafeccion(), comunidad[i].getprob_morir(),afecc_obs,afecc_des);
			comunidad[i].setprob_morir(prob_muerte);
			
			prob = azar.probabilidad();
			if(prob <= enfer.getprob_infec_inicial()) {
				comunidad[i].setinfectado(2);
			}
		}

		//Crear comunidades cercanas
		for (int i = 0; i < comun.getnum_perso(); ++i) {
			if(comunidad[i].getcomunidad() == 0) {
				int max = azar.max_integrantes_comunidad();
				int conteiner = 0;
				for (int indice = 0; indice < comun.getnum_perso(); ++indice) {
					if(comunidad[indice].getcomunidad() == 0) {
						comunidad[indice].setcomunidad(comun.getnum_comunidad());
						conteiner = conteiner + 1;
						if (conteiner == max) {
							comun.setnum_comunidad(comun.getnum_comunidad()+1);
							break;
		} } } } }
	}
	
	// ----------------------------------------------------------------------------
	// desarrollo de la simulacion
	// ----------------------------------------------------------------------------
	
	public void pasos() {
		steps.setContador(1);
		while(steps.getContador() != steps.getPromedio_pasos()) {
			for (int i = 0; i < comun.getnum_perso(); ++i) {
				if (comunidad[i].getestado()) {
					if (comunidad[i].getinfectado() == 2) {
						int prob = azar.probabilidad();
						if(prob <= comun.getprob_conec_fsc()) {
							contacto_ciudadanos(i);
						}
						prob = azar.probabilidad();
						if(prob <= comun.getprob_conec_fsc_comunidad()) {
							contacto_comunidad_cercana(i);
						}
					}
					if (comunidad[i].gettiempo_infec() >= steps.getmutacion()) {
						if (comunidad[i].getinfectado() == 2 || comunidad[i].getinfectado() == 3) {
							muerte_cura_mantener(i);
						}
					}
					if (comunidad[i].gettiempo_infec() >= steps.gettiemp_leve_grave() && comunidad[i].getinfectado() == 2 && comunidad[i].getprob_morir() > 0) {
						comunidad[i].setinfectado(3);
						if(60 > comunidad[i].getprob_morir()) {
							comunidad[i].setprob_morir(60);
						}
					}
					
					if (steps.getContador() == par_vac.getotorgar()) {
						for (int indice = 0; indice < comun.getnum_perso(); ++indice) {
							if (comunidad[indice].getestado()) {
								if (0 < vac1.getcantidad()) {
									if (comunidad[indice].getinmaculado() == 0) {
										comunidad[indice].setinmaculado(1);
										comunidad[indice].setprob_morir(comunidad[indice].getprob_morir()-vac1.getefecto_prob_muerte());
										vac1.setcantidad(vac1.getcantidad()-1);
										comunidad[indice].settiempo_infec(0);
									}
								}
								if (0 < vac2.getcantidad()) {
									if (comunidad[indice].getinmaculado() == 0) {
										comunidad[indice].setinmaculado(2);
										vac2.setcantidad(vac2.getcantidad()-1);
									}
								}
								if (0 < vac3.getcantidad()) {
									if (comunidad[indice].getinmaculado() == 0) {
										comunidad[indice].setinmaculado(3);
										vac3.setcantidad(vac3.getcantidad()-1);
									}
								}
							}
						}	
					}if (steps.getContador() == par_vac.getotorgar()+vac2.getpasos_segunda_dosis()) {
						for (int indice = 0; indice < comun.getnum_perso(); ++indice) {
							if (comunidad[indice].getestado()) {
								if (comunidad[indice].getinmaculado() == 2) {
									comunidad[indice].setprob_morir(vac2.getefecto_prob_muerte());
									comunidad[indice].setinfectado(2);
								}
							}
						}
					}if (steps.getContador() == par_vac.getotorgar()+vac3.getpasos_segunda_dosis()) {
						for (int indice = 0; indice < comun.getnum_perso(); ++indice) {
							if (comunidad[indice].getestado()) {
								if (comunidad[indice].getinmaculado() == 3) {
									comunidad[indice].setprob_morir(vac3.getefecto_prob_muerte());
									comunidad[indice].setinfectado(4);
								}
							}
						}
					}
					
					
					if (comunidad[i].getinfectado() == 2) {
						comunidad[i].settiempo_infec(comunidad[i].gettiempo_infec()+1);
					}
				}
			}
			int total_infectados = evaluacion_periodica(steps.getContador());
			if(total_infectados == 0) {
				steps.setDuracion(steps.getContador());
				steps.setContador(steps.getPromedio_pasos());
			}else {
				steps.setContador(steps.getContador()+1);
			}
		}
	}
	
	// ----------------------------------------------------------------------------
	// un ciudadano puede infectar a uno al azar o ser infectado por uno al azar, tambien se puede dar de que no pase nada
	// ----------------------------------------------------------------------------
	
	public void contacto_ciudadanos(int involucrado) {
		int persona_azar = azar.probabilidad_ciudadano(comun.getnum_perso());
		int prob = azar.probabilidad();
		if(comunidad[persona_azar].getinfectado() == 2 && comunidad[involucrado].getinfectado() == 1) {
			if(prob <= enfer.getInfeccion_probable()) {
				if(60 <= comunidad[involucrado].getprob_morir()) {
					comunidad[involucrado].setinfectado(3);
				}else {
					comunidad[involucrado].setinfectado(2);
				}
			}
		}if (comunidad[persona_azar].getinfectado() == 1 && comunidad[involucrado].getinfectado() == 2) {
			if(prob <= enfer.getInfeccion_probable()) {
				if(60 <= comunidad[persona_azar].getprob_morir()) {
					comunidad[persona_azar].setinfectado(3);
				}else {
					comunidad[persona_azar].setinfectado(2);
				}
			}
		}	
	}
	
	public void contacto_comunidad_cercana(int involucrado) {
		int prob = azar.probabilidad();
		if(prob <= enfer.getInfeccion_probable()) {
			for (int i = 0; i < comun.getnum_perso(); ++i) {
				if (comunidad[i].getcomunidad() == comunidad[involucrado].getcomunidad()) {
					if (comunidad[i].getinfectado() == 1) {
						if(60 <= comunidad[i].getprob_morir()) {
							comunidad[i].setinfectado(3);
						}else {
							comunidad[i].setinfectado(2);
						}
					}
				}
			}
		}
	}
	
	// ----------------------------------------------------------------------------
	// desarrollo de los infectados: se pueden volver inmune, morir o mantenerse vivo e infectado
	// ----------------------------------------------------------------------------
	
	public void muerte_cura_mantener(int involucrado) {
		int prob = azar.probabilidad();
		if (comunidad[involucrado].gettiempo_infec() >= steps.getmutacion()) {
			if (prob <= enfer.getprob_gener_inmunidad()) {
				//curado e inmune
				comunidad[involucrado].setinfectado(4);
			
			}
			prob = azar.probabilidad();
			if (prob <= comunidad[involucrado].getprob_morir()) {
				// muerto
				comunidad[involucrado].setinfectado(4);
				comunidad[involucrado].setestado(false);
			}else{
				//se mantiene vivo e infectado
			}
		}
	}
	
	// --------------------------------------------------------------------------------------
	// evaluaciones de infectados y casos activos en 3 fases de la simulacion: inicio, desarrollo y final
	// --------------------------------------------------------------------------------------
	
	//inicio
	public void evaluacion_inicial() {
		int cnt_enfer_y_afecc = 0;
		int cnt_enfer_o_afecc = 0;
		int cnt_enfer = 0;
		int cnt_afecc = 0;
		int cnt_infec_ini = 0;
		
		double sumatoria_edad = 0;
		
		System.out.println("Se inicia simulacion con: ");
		System.out.println("Total de poblacion: "+comun.getnum_perso());
		
		for (int i = 0; i < comun.getnum_perso(); ++i) {
			if(comunidad[i].getinfectado() == 2) {
				cnt_infec_ini++;
			}
			if(comunidad[i].getenfer_base() > 1) {
				cnt_enfer++;
			}
			if (comunidad[i].getafeccion() > 1) {
				cnt_afecc++;
			}
			if (comunidad[i].getenfer_base() > 1 || comunidad[i].getafeccion() > 1) {
				cnt_enfer_o_afecc++;
			}
			if (comunidad[i].getenfer_base() > 1 && comunidad[i].getafeccion() > 1) {
				cnt_enfer_y_afecc++;
			}
			sumatoria_edad = sumatoria_edad + comunidad[i].getedad();
		}
		sumatoria_edad =(int)(sumatoria_edad/comun.getnum_perso());
		System.out.println("Total de poblacion que padece afeccion o enfermedad base: "+cnt_enfer_o_afecc);
		System.out.println("Total de poblacion que padece afeccion y enfermedad base: "+cnt_enfer_y_afecc);
		System.out.println("Total de poblacion que padece enfermedad base: "+cnt_enfer);
		System.out.println("Total de poblacion que padece afeccion: "+cnt_afecc);
		System.out.println("Total de poblacion con enfermedad infecciosa inicial: "+cnt_infec_ini);
		System.out.println("");
		System.out.println("Edad promedio de la comunidad: "+ sumatoria_edad);
		int vacunas_total = vac1.getcantidad()+vac2.getcantidad()+vac3.getcantidad();
		System.out.println("Vacunas disponibles: "+ vacunas_total);
		System.out.println("");
		
		int prob = azar.probabilidad_ciudadano(comun.getnum_perso());
		System.out.println("Caracteristicas de una persona al azar (ID): "+comunidad[prob].getid());
		System.out.println("Edad: "+comunidad[prob].getedad());
		if (comunidad[prob].getinfectado() == 2) {
			System.out.println("Infectado inicial: SI");
		}else {
			System.out.println("Infectado inicial: NO");
		}if (comunidad[prob].getenfer_base() == 1) {
			System.out.println("Enfermedad inicial: NO");
		}else if (comunidad[prob].getenfer_base() != enfer_base_asma.getEnfermo()) {
			System.out.println("Enfermedad inicial: SI ("+enfer_base_asma.getpatologia()+")");
		}else if (comunidad[prob].getenfer_base() != enfer_base_ecv.getEnfermo()) {
			System.out.println("Enfermedad inicial: SI ("+enfer_base_ecv.getpatologia()+")");
		}else if (comunidad[prob].getenfer_base() != enfer_base_fq.getEnfermo()) {
			System.out.println("Enfermedad inicial: SI ("+enfer_base_fq.getpatologia()+")");
		}else if (comunidad[prob].getenfer_base() != enfer_base_hiper.getEnfermo()) {
			System.out.println("Enfermedad inicial: SI ("+enfer_base_hiper.getpatologia()+")");
		}else if (comunidad[prob].getenfer_base() != enfer_base_pal.getEnfermo()) {
			System.out.println("Enfermedad inicial: SI ("+enfer_base_pal.getpatologia()+")");
		}if (comunidad[prob].getafeccion() == 1) {
			System.out.println("Afeccion inicial: NO");
		}else if (comunidad[prob].getafeccion() == afecc_obs.getEnfermo()) {
			System.out.println("Afeccion inicial: SI ("+afecc_obs.getpatologia()+")");
		}else if (comunidad[prob].getafeccion() == afecc_des.getEnfermo()) {
			System.out.println("Afeccion inicial: SI ("+afecc_des.getpatologia()+")");
		}
		System.out.println("Probabilidad de morir: "+comunidad[prob].getprob_morir());

		
		System.out.println("");
		
		int cnt_infec = 0;
		int cnt_infec_grave = 0;
		int cnt_inmune = 0;
		for (int i = 0; i < comun.getnum_perso(); ++i) {
			if(comunidad[i].getinfectado() == 2) {
				cnt_infec++;
			}if(comunidad[i].getprob_morir() == 3) {
				cnt_infec++;
				cnt_infec_grave++;
			}if(comunidad[i].getinfectado() == 4) {
				cnt_inmune++;
			}
		}
		comun.setnum_infec(cnt_infec);
		System.out.println("Desarrollo de la simulación: ");
		System.out.println("Paso: 0, Infectados totales: "+cnt_infec+", Casos activos: "+cnt_infec+", Infectados graves: "+cnt_infec_grave+", Infectados recuperados: "+cnt_inmune);
	}
	
	//desarrollo
	public int evaluacion_periodica(int step) {
		int cnt_activos = 0;
		int cnt_inmune = 0;
		int cnt_infec = 0;
		int cnt_infec_grave = 0;
		for (int i = 0; i < comun.getnum_perso(); ++i) {
			
			if(comunidad[i].getinfectado() == 2) {
				cnt_infec++;
				cnt_activos++;
			}if(comunidad[i].getinfectado() == 3) {
				cnt_infec++;
				cnt_activos++;
				cnt_infec_grave++;
			}if(comunidad[i].getinfectado() == 4 && comunidad[i].getestado()) {
				cnt_infec++;
				cnt_inmune++;
			}if(comunidad[i].getinfectado() == 4 && !comunidad[i].getestado()) {
				cnt_infec++;
			}
		}
		System.out.println("Paso: "+step+", Infectados totales: "+cnt_infec+", Casos activos: "+cnt_activos+", Infectados graves: "+cnt_infec_grave+", Infectados recuperados: "+cnt_inmune);
		return cnt_activos;
	}
	
	//final
	public void evaluacion_final() {
		int cnt_vida = 0;
		int cnt_muerte = 0;
		int cnt_no_infec = 0;
		int cnt_inmune = 0;
		int cnt_infec = 0;
		int cnt_infec_leve = 0;
		int cnt_infec_grave = 0;
		int cnt_inmaculados = 0;
		int cnt_inmaculados_vac1 = 0;
		int cnt_inmaculados_vac2 = 0;
		int cnt_inmaculados_vac3 = 0;
		double sumatoria_edad = 0;
		
		for (int i = 0; i < comun.getnum_perso(); ++i) {
			if (comunidad[i].getinmaculado() == 1) {
				cnt_inmaculados++;
				cnt_inmaculados_vac1++;
			}if (comunidad[i].getinmaculado() == 2) {
				cnt_inmaculados++;
				cnt_inmaculados_vac2++;
			}if (comunidad[i].getinmaculado() == 3) {
				cnt_inmaculados++;
				cnt_inmaculados_vac3++;
			}
			if(comunidad[i].getestado() == true) {
				sumatoria_edad = sumatoria_edad + comunidad[i].getedad();
				if (comunidad[i].getinfectado() == 1) {
					cnt_vida++;
					cnt_no_infec++;
				}if (comunidad[i].getinfectado() == 2) {
					cnt_vida++;
					cnt_infec++;
					cnt_infec_leve++;
				}if (comunidad[i].getinfectado() == 3) {
					cnt_vida++;
					cnt_infec++;
					cnt_infec_grave++;
				}if (comunidad[i].getinfectado() == 4) {
					cnt_vida++;
					cnt_inmune++;
				}
			}else if (!comunidad[i].getestado()) {
				cnt_muerte++;
			}
		}
		sumatoria_edad = (int)(sumatoria_edad/cnt_vida);
		System.out.println("<========================================================================================>");
		System.out.println("Al pasar "+steps.getContador()+" dias de los "+steps.getPromedio_pasos()+" totales a simular, la enfermedad registra lo siguiente: ");
		System.out.println("Totalidad de población = "+ comun.getnum_perso());
		System.out.println("Totalidad de personas vivas = "+ cnt_vida);
		System.out.println("Totalidad de personas muertas = "+ cnt_muerte);
		System.out.println("");
		System.out.println("Personas vivas y sanas: "+ cnt_no_infec);
		System.out.println("Personas vivas e infectadas: "+ cnt_infec);
		System.out.println("");
		System.out.println("Personas vivas e infectadas (leves): "+ cnt_infec_leve);
		System.out.println("Personas vivas e infectadas (graves): "+ cnt_infec_grave);
		System.out.println("");
		System.out.println("Personas vivas e inmunes: "+ cnt_inmune);
		System.out.println("Personas inmaculadas: "+ cnt_inmaculados);
		System.out.println("Personas inmaculadas (vacuna 1): "+ cnt_inmaculados_vac1);
		System.out.println("Personas inmaculadas (vacuna 2): "+ cnt_inmaculados_vac2);
		System.out.println("Personas inmaculadas (vacuna 3): "+ cnt_inmaculados_vac3);
		System.out.println("");
		System.out.println("Edad promedio de personas vivas: "+sumatoria_edad);
	}
}