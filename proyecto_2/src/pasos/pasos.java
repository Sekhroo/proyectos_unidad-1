package pasos;

public class pasos {
	
	private int promedio_pasos; // promedio de tiempo que se simula
	private int mutacion; //tiempo que tarda la enfermedad en generar una mutacion que mata al sistema o este se vuelve inmune
	private int tiemp_leve_grave; //tiwmpo que tiene que pasar leve para pasar a grave
	private int contador = 0; // tiempo de en que va el desarrollo de la simulacion
	private int duracion = 0; // duracion total de la simulacion, en caso de que no llegue al promedio_pasos
	
	public pasos(int promedio_pasos, int mutacion, int tiemp_leve_grave) {
		this.promedio_pasos = promedio_pasos;
		this.mutacion = mutacion;
		this.tiemp_leve_grave = tiemp_leve_grave;
	}
	
	// ----------------------------------------------------------------------------
	// getters y setters
	// ----------------------------------------------------------------------------
	
    public int getPromedio_pasos() {
        return this.promedio_pasos;
    }
    public void setPromedio_pasos(int promedio_pasos) {
        this.promedio_pasos = promedio_pasos;
    }
    
    public int getContador() {
        return this.contador;
    }
    public void setContador(int contador) {
        this.contador = contador;
    }
    
    public int getDuracion() {
        return this.duracion;
    }
    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
    
    public int getmutacion() {
        return this.mutacion;
    }
    public void setmutacion(int mutacion) {
        this.mutacion = mutacion;
    }
    
    public int gettiemp_leve_grave() {
        return this.tiemp_leve_grave;
    }
    public void settiemp_leve_grave(int tiemp_leve_grave) {
        this.tiemp_leve_grave = tiemp_leve_grave;
    }
}