package enfermedades;

public class enfer_base extends enfermedad {
	
	private String patologia;
	private int prob_morir;
	
//  infeccion:
//	1 = sano
//	2 = asma
//	3 = enfermedad cerebrovascular
//	4 = fibrosis quistica
//	5 = hipertension
//	6 = presion arterial alta
	
	public enfer_base(String patologia,int prob_infec_inicial,int enfermo,int prob_morir) {
		super(prob_infec_inicial,enfermo);
		this.patologia = patologia;
		this.prob_morir = prob_morir;

	}
	
	public String getpatologia() {
        return this.patologia;
    }
    public void setpatologia(String patologia) {
        this.patologia = patologia;
    }
    
    public int getprob_morir() {
        return this.prob_morir;
    }
    public void setprob_morir(int prob_morir) {
        this.prob_morir = prob_morir;
    }
}