package enfermedades;

public class enfer_infecciosa extends enfermedad {
	
//  infeccion:
//	1 = sano
//	2 = infeccioso
//	3 = no infeccioso
	
	private int infeccion_probable; // si hay contacto fisico, probabilidad de contagiarse
	private int prob_gener_inmunidad; // probabilidad de que el infectado genere inmunidad
	
	public enfer_infecciosa(int prob_infec_inicial,int enfermo, int infeccion_probable, int prob_gener_inmunidad) {
		super(prob_infec_inicial,enfermo);
		this.infeccion_probable = infeccion_probable;
		this.prob_gener_inmunidad = prob_gener_inmunidad;
	}
	
	// ----------------------------------------------------------------------------
	// getters y setters
	// ----------------------------------------------------------------------------
    
    public int getInfeccion_probable() {
        return this.infeccion_probable;
    }
    public void setInfeccion_probable(int infeccion_probable) {
        this.infeccion_probable = infeccion_probable;
    }
    
    public int getprob_gener_inmunidad() {
        return this.prob_gener_inmunidad;
    }
    public void setprob_gener_inmunidad(int prob_gener_inmunidad) {
        this.prob_gener_inmunidad = prob_gener_inmunidad;
    }
}