package enfermedades;


public class enfermedad {
	
	private int prob_infec_inicial; // probabilidad de afeccion inicial
	private int enfermo;

	
	public enfermedad(int prob_infec_inicial, int enfermo) {

		this.prob_infec_inicial = prob_infec_inicial;
		this.enfermo = enfermo;
	}
	
	// ----------------------------------------------------------------------------
	// getters y setters
	// ----------------------------------------------------------------------------
	
    public int getEnfermo() {
        return this.enfermo;
    }
    public void setEnfermo(int enfermo) {
        this.enfermo = enfermo;
    }
    
    public int getprob_infec_inicial() {
        return this.prob_infec_inicial;
    }
    public void setprob_infec_inicial(int prob_infec_inicial) {
        this.prob_infec_inicial = prob_infec_inicial;
    }
}