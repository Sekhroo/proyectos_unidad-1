package probabilidad_azar;

import simulador.ciudadano;

public class probabilidad {
	
	// ----------------------------------------------------------------------------
	// Probabilidad del 1 al 100
	// ----------------------------------------------------------------------------
	
	public int probabilidad() {
		int prob = (int) (Math.random()*100+1);
        return 	prob;
	}
	
	// ----------------------------------------------------------------------------
	// Probabilidad del 2 al 5 (Integrantes de una familia)
	// ----------------------------------------------------------------------------
	
    public int max_integrantes_comunidad() {
		int max = (int) (Math.random()*4+2);
        return 	max;
	}
    
	// ----------------------------------------------------------------------------
	// Probabilidad del 0 a el maximo de poblacion
	// ----------------------------------------------------------------------------
	
	public int probabilidad_ciudadano(int poblacion) {
		int ciudadano_azar = (int) (Math.random()*poblacion);
        return ciudadano_azar;
	}
}